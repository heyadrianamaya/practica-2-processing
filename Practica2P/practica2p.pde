import gab.opencv.*;
import java.awt.Rectangle;
import processing.video.*;

Capture video;

OpenCV opencv;
PImage fox;
Rectangle[] faces;

void setup(){
    size(1080, 720);
    
    video = new Capture(this, 1280, 720, "DroidCam Source 3");
    video.start();
    fox = loadImage("donald.png");

    opencv = new OpenCV(this, video);

    opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);
}

void draw(){
    if (video.available() == true) {
        video.read();
        opencv.loadImage(video);
        
        image(video, 0,0);
        
        noFill();
        stroke(0, 255, 0);
        strokeWeight(3);
        faces = opencv.detect();
        
        for (int i = 0; i < faces.length; i++) {
            // rect(faces[i].x, faces[i].y, faces[i].width, faces[i]. height);
            image(fox, faces[i].x  - 40 - faces[i].width / 2, faces[i].y - faces[i].height / 2, faces[i].width * 2 + 30, faces[i].height * 2);
        }
    }
}

